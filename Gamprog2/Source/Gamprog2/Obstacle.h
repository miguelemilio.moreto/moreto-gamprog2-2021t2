// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Obstacle.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FHitObstacleSignature, class ARunCharacter*, RunCharacter);
UCLASS()

class GAMPROG2_API AObstacle : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(BlueprintAssignable)
	FHitObstacleSignature HitObstacle;
	 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class UStaticMeshComponent* StaticMesh;

	UFUNCTION()
	void OnObstacleHit( UPrimitiveComponent*HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	// change variables inside 

	// Sets default values for this actor's properties
	AObstacle();

};

// Implimentable event 