// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EndlessRunner.generated.h"

/**
 * 
 */
UCLASS()
class GAMPROG2_API AEndlessRunner : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, Category = "Config")
		TSubclassOf<class AFloorTile> FloorTileClass;

	UPROPERTY(EditAnywhere, Category = "Config")
		int32 NumInitialFloorTiles = 10;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 TotalCoins = 0;

	UPROPERTY(VisibleInstanceOnly, Category = "Runtime")
		FTransform NextSpawnPoint;

	UFUNCTION(BlueprintCallable)
		void CreateIntialFloorTiles();

	UFUNCTION(BlueprintCallable)
		void AddFloorTiles(const bool bSpawnItems);

	UFUNCTION(BlueprintCallable)
		void AddCoin();

	UFUNCTION()
		void RestartLevel();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnTileExited(class ARunCharacter* RunCharacter);

	UFUNCTION()
	void HitObstacle(class ARunCharacter* RunCharacter);
};

// dispatcher for tile and mode
