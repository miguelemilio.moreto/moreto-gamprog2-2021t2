// Copyright Epic Games, Inc. All Rights Reserved.

#include "Gamprog2GameMode.h"
#include "Gamprog2Character.h"
#include "UObject/ConstructorHelpers.h"

AGamprog2GameMode::AGamprog2GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
