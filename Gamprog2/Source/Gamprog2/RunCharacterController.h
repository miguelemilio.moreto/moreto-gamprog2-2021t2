// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RunCharacter.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"


/**
 * 
 */
UCLASS()
class GAMPROG2_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ARunCharacterController();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

protected:
	// Called when the game starts or when spawned
	 virtual void BeginPlay() override;

	 ARunCharacter* Player;

	 void MoveRight(float scale);
	 void MoveForward(float scale);


};
