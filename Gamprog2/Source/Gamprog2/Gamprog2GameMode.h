// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Gamprog2GameMode.generated.h"

UCLASS(minimalapi)
class AGamprog2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGamprog2GameMode();
};



