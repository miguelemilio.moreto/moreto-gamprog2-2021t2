// Fill out your copyright notice in the Description page of Project Settings.


#include "EndlessRunner.h"
#include "FloorTile.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Kismet/GameplayStatics.h"


void AEndlessRunner::BeginPlay()
{
	CreateIntialFloorTiles();

	if (AFloorTile* FloorTile = Cast<AFloorTile>(GetWorld()->GetFirstPlayerController()->GetPawn()))
	{
		FloorTile->OnTileExited.AddDynamic(this, &AEndlessRunner::OnTileExited);
	}

	if (AObstacle* Obstacle = Cast<AObstacle>(GetWorld()->GetFirstPlayerController()->GetPawn()))
	{
		Obstacle->HitObstacle.AddDynamic(this, &AEndlessRunner::HitObstacle);
	}

}

void AEndlessRunner::OnTileExited(ARunCharacter* RunCharacter)
{
	AddFloorTiles(true);
	//GetWorldTimerManager().SetTimer(DestroyHandle, this, &AFloorTile::DestroyFloorTile, 2.f, false);
}

void AEndlessRunner::HitObstacle(ARunCharacter* RunCharacter)
{
	RunCharacter->Death();
	RestartLevel();
}

void AEndlessRunner::CreateIntialFloorTiles()
{
	for (int i = 0; i < NumInitialFloorTiles; i++)
	{
		AddFloorTiles(true);
	}
}

void AEndlessRunner::AddFloorTiles(const bool bSpawnItems)
{
	UWorld* World = GetWorld();

	if (World)
	{
		AFloorTile* Tile = World->SpawnActor<AFloorTile>(FloorTileClass, NextSpawnPoint);

		if (Tile)
		{
			if (bSpawnItems)
			{
				Tile->SpawnItems();
			}

			NextSpawnPoint = Tile->GetAttachTransform(); 
		}
	}
}

void AEndlessRunner::AddCoin()
{
	/*TotalCoins += 1;

	UE_LOG(LogTemp, Warning, TEXT("Total Coins: %d"), TotalCoins);*/
}

void AEndlessRunner::RestartLevel()
{
	UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(), TEXT("RestartLevel"));

}

