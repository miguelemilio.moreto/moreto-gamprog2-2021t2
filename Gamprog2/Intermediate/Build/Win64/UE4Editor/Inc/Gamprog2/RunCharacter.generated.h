// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2_RunCharacter_generated_h
#error "RunCharacter.generated.h already included, missing '#pragma once' in RunCharacter.h"
#endif
#define GAMPROG2_RunCharacter_generated_h

#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_SPARSE_DATA
#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnDeath); \
	DECLARE_FUNCTION(execAddCoin); \
	DECLARE_FUNCTION(execDeath);


#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnDeath); \
	DECLARE_FUNCTION(execAddCoin); \
	DECLARE_FUNCTION(execDeath);


#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacter(); \
	friend struct Z_Construct_UClass_ARunCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacter)


#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacter(); \
	friend struct Z_Construct_UClass_ARunCharacter_Statics; \
public: \
	DECLARE_CLASS(ARunCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacter)


#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacter(ARunCharacter&&); \
	NO_API ARunCharacter(const ARunCharacter&); \
public:


#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacter(ARunCharacter&&); \
	NO_API ARunCharacter(const ARunCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacter)


#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraArm() { return STRUCT_OFFSET(ARunCharacter, CameraArm); } \
	FORCEINLINE static uint32 __PPO__Camera() { return STRUCT_OFFSET(ARunCharacter, Camera); } \
	FORCEINLINE static uint32 __PPO__RunGameMode() { return STRUCT_OFFSET(ARunCharacter, RunGameMode); } \
	FORCEINLINE static uint32 __PPO__RestartTimerHandle() { return STRUCT_OFFSET(ARunCharacter, RestartTimerHandle); } \
	FORCEINLINE static uint32 __PPO__bIsDead() { return STRUCT_OFFSET(ARunCharacter, bIsDead); }


#define Gamprog2_Source_Gamprog2_RunCharacter_h_11_PROLOG
#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_RPC_WRAPPERS \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_INCLASS \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamprog2_Source_Gamprog2_RunCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_INCLASS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_RunCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2_API UClass* StaticClass<class ARunCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamprog2_Source_Gamprog2_RunCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
