// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2_SampleActor_generated_h
#error "SampleActor.generated.h already included, missing '#pragma once' in SampleActor.h"
#endif
#define GAMPROG2_SampleActor_generated_h

#define Gamprog2_Source_Gamprog2_SampleActor_h_12_SPARSE_DATA
#define Gamprog2_Source_Gamprog2_SampleActor_h_12_RPC_WRAPPERS
#define Gamprog2_Source_Gamprog2_SampleActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Gamprog2_Source_Gamprog2_SampleActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASampleActor(); \
	friend struct Z_Construct_UClass_ASampleActor_Statics; \
public: \
	DECLARE_CLASS(ASampleActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(ASampleActor)


#define Gamprog2_Source_Gamprog2_SampleActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASampleActor(); \
	friend struct Z_Construct_UClass_ASampleActor_Statics; \
public: \
	DECLARE_CLASS(ASampleActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(ASampleActor)


#define Gamprog2_Source_Gamprog2_SampleActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASampleActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASampleActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASampleActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASampleActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASampleActor(ASampleActor&&); \
	NO_API ASampleActor(const ASampleActor&); \
public:


#define Gamprog2_Source_Gamprog2_SampleActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASampleActor(ASampleActor&&); \
	NO_API ASampleActor(const ASampleActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASampleActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASampleActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASampleActor)


#define Gamprog2_Source_Gamprog2_SampleActor_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMesh() { return STRUCT_OFFSET(ASampleActor, StaticMesh); } \
	FORCEINLINE static uint32 __PPO__curHp() { return STRUCT_OFFSET(ASampleActor, curHp); } \
	FORCEINLINE static uint32 __PPO__maxHp() { return STRUCT_OFFSET(ASampleActor, maxHp); }


#define Gamprog2_Source_Gamprog2_SampleActor_h_9_PROLOG
#define Gamprog2_Source_Gamprog2_SampleActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_RPC_WRAPPERS \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_INCLASS \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamprog2_Source_Gamprog2_SampleActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_INCLASS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_SampleActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2_API UClass* StaticClass<class ASampleActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamprog2_Source_Gamprog2_SampleActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
