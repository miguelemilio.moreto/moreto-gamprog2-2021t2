// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ARunCharacter;
#ifdef GAMPROG2_EndlessRunner_generated_h
#error "EndlessRunner.generated.h already included, missing '#pragma once' in EndlessRunner.h"
#endif
#define GAMPROG2_EndlessRunner_generated_h

#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_SPARSE_DATA
#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHitObstacle); \
	DECLARE_FUNCTION(execOnTileExited); \
	DECLARE_FUNCTION(execRestartLevel); \
	DECLARE_FUNCTION(execAddCoin); \
	DECLARE_FUNCTION(execAddFloorTiles); \
	DECLARE_FUNCTION(execCreateIntialFloorTiles);


#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHitObstacle); \
	DECLARE_FUNCTION(execOnTileExited); \
	DECLARE_FUNCTION(execRestartLevel); \
	DECLARE_FUNCTION(execAddCoin); \
	DECLARE_FUNCTION(execAddFloorTiles); \
	DECLARE_FUNCTION(execCreateIntialFloorTiles);


#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlessRunner(); \
	friend struct Z_Construct_UClass_AEndlessRunner_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunner, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(AEndlessRunner)


#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAEndlessRunner(); \
	friend struct Z_Construct_UClass_AEndlessRunner_Statics; \
public: \
	DECLARE_CLASS(AEndlessRunner, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(AEndlessRunner)


#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEndlessRunner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlessRunner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlessRunner(AEndlessRunner&&); \
	NO_API AEndlessRunner(const AEndlessRunner&); \
public:


#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEndlessRunner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlessRunner(AEndlessRunner&&); \
	NO_API AEndlessRunner(const AEndlessRunner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlessRunner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlessRunner); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlessRunner)


#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_PRIVATE_PROPERTY_OFFSET
#define Gamprog2_Source_Gamprog2_EndlessRunner_h_12_PROLOG
#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_RPC_WRAPPERS \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_INCLASS \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamprog2_Source_Gamprog2_EndlessRunner_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_INCLASS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_EndlessRunner_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2_API UClass* StaticClass<class AEndlessRunner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamprog2_Source_Gamprog2_EndlessRunner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
