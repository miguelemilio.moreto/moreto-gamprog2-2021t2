// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ARunCharacter;
class UArrowComponent;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef GAMPROG2_FloorTile_generated_h
#error "FloorTile.generated.h already included, missing '#pragma once' in FloorTile.h"
#endif
#define GAMPROG2_FloorTile_generated_h

#define Gamprog2_Source_Gamprog2_FloorTile_h_17_DELEGATE \
struct _Script_Gamprog2_eventTileExitedSignature_Parms \
{ \
	ARunCharacter* RunCharacter; \
}; \
static inline void FTileExitedSignature_DelegateWrapper(const FMulticastScriptDelegate& TileExitedSignature, ARunCharacter* RunCharacter) \
{ \
	_Script_Gamprog2_eventTileExitedSignature_Parms Parms; \
	Parms.RunCharacter=RunCharacter; \
	TileExitedSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Gamprog2_Source_Gamprog2_FloorTile_h_22_SPARSE_DATA
#define Gamprog2_Source_Gamprog2_FloorTile_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnLaneItem); \
	DECLARE_FUNCTION(execDestroyFloorTile); \
	DECLARE_FUNCTION(execOnTriggerBoxOverlap); \
	DECLARE_FUNCTION(execSpawnItems);


#define Gamprog2_Source_Gamprog2_FloorTile_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnLaneItem); \
	DECLARE_FUNCTION(execDestroyFloorTile); \
	DECLARE_FUNCTION(execOnTriggerBoxOverlap); \
	DECLARE_FUNCTION(execSpawnItems);


#define Gamprog2_Source_Gamprog2_FloorTile_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFloorTile(); \
	friend struct Z_Construct_UClass_AFloorTile_Statics; \
public: \
	DECLARE_CLASS(AFloorTile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(AFloorTile)


#define Gamprog2_Source_Gamprog2_FloorTile_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAFloorTile(); \
	friend struct Z_Construct_UClass_AFloorTile_Statics; \
public: \
	DECLARE_CLASS(AFloorTile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(AFloorTile)


#define Gamprog2_Source_Gamprog2_FloorTile_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFloorTile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFloorTile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloorTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloorTile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloorTile(AFloorTile&&); \
	NO_API AFloorTile(const AFloorTile&); \
public:


#define Gamprog2_Source_Gamprog2_FloorTile_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFloorTile(AFloorTile&&); \
	NO_API AFloorTile(const AFloorTile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFloorTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFloorTile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFloorTile)


#define Gamprog2_Source_Gamprog2_FloorTile_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EndlessRunnerMode() { return STRUCT_OFFSET(AFloorTile, EndlessRunnerMode); } \
	FORCEINLINE static uint32 __PPO__DestroyHandle() { return STRUCT_OFFSET(AFloorTile, DestroyHandle); }


#define Gamprog2_Source_Gamprog2_FloorTile_h_19_PROLOG
#define Gamprog2_Source_Gamprog2_FloorTile_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_RPC_WRAPPERS \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_INCLASS \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamprog2_Source_Gamprog2_FloorTile_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_INCLASS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_FloorTile_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2_API UClass* StaticClass<class AFloorTile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamprog2_Source_Gamprog2_FloorTile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
