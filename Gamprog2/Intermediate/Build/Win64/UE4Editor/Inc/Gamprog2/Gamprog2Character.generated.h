// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2_Gamprog2Character_generated_h
#error "Gamprog2Character.generated.h already included, missing '#pragma once' in Gamprog2Character.h"
#endif
#define GAMPROG2_Gamprog2Character_generated_h

#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_SPARSE_DATA
#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_RPC_WRAPPERS
#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGamprog2Character(); \
	friend struct Z_Construct_UClass_AGamprog2Character_Statics; \
public: \
	DECLARE_CLASS(AGamprog2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(AGamprog2Character)


#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGamprog2Character(); \
	friend struct Z_Construct_UClass_AGamprog2Character_Statics; \
public: \
	DECLARE_CLASS(AGamprog2Character, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(AGamprog2Character)


#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGamprog2Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGamprog2Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGamprog2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGamprog2Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGamprog2Character(AGamprog2Character&&); \
	NO_API AGamprog2Character(const AGamprog2Character&); \
public:


#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGamprog2Character(AGamprog2Character&&); \
	NO_API AGamprog2Character(const AGamprog2Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGamprog2Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGamprog2Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGamprog2Character)


#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(AGamprog2Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(AGamprog2Character, FollowCamera); }


#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_9_PROLOG
#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_RPC_WRAPPERS \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_INCLASS \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_INCLASS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_Gamprog2Character_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2_API UClass* StaticClass<class AGamprog2Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamprog2_Source_Gamprog2_Gamprog2Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
