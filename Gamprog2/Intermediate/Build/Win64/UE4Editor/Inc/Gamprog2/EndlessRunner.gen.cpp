// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Gamprog2/EndlessRunner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEndlessRunner() {}
// Cross Module References
	GAMPROG2_API UClass* Z_Construct_UClass_AEndlessRunner_NoRegister();
	GAMPROG2_API UClass* Z_Construct_UClass_AEndlessRunner();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_Gamprog2();
	GAMPROG2_API UClass* Z_Construct_UClass_ARunCharacter_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	GAMPROG2_API UClass* Z_Construct_UClass_AFloorTile_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AEndlessRunner::execHitObstacle)
	{
		P_GET_OBJECT(ARunCharacter,Z_Param_RunCharacter);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HitObstacle(Z_Param_RunCharacter);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AEndlessRunner::execOnTileExited)
	{
		P_GET_OBJECT(ARunCharacter,Z_Param_RunCharacter);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnTileExited(Z_Param_RunCharacter);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AEndlessRunner::execRestartLevel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RestartLevel();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AEndlessRunner::execAddCoin)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddCoin();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AEndlessRunner::execAddFloorTiles)
	{
		P_GET_UBOOL(Z_Param_bSpawnItems);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddFloorTiles(Z_Param_bSpawnItems);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AEndlessRunner::execCreateIntialFloorTiles)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CreateIntialFloorTiles();
		P_NATIVE_END;
	}
	void AEndlessRunner::StaticRegisterNativesAEndlessRunner()
	{
		UClass* Class = AEndlessRunner::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddCoin", &AEndlessRunner::execAddCoin },
			{ "AddFloorTiles", &AEndlessRunner::execAddFloorTiles },
			{ "CreateIntialFloorTiles", &AEndlessRunner::execCreateIntialFloorTiles },
			{ "HitObstacle", &AEndlessRunner::execHitObstacle },
			{ "OnTileExited", &AEndlessRunner::execOnTileExited },
			{ "RestartLevel", &AEndlessRunner::execRestartLevel },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AEndlessRunner_AddCoin_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEndlessRunner_AddCoin_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AEndlessRunner_AddCoin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AEndlessRunner, nullptr, "AddCoin", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AEndlessRunner_AddCoin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_AddCoin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AEndlessRunner_AddCoin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AEndlessRunner_AddCoin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics
	{
		struct EndlessRunner_eventAddFloorTiles_Parms
		{
			bool bSpawnItems;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSpawnItems_MetaData[];
#endif
		static void NewProp_bSpawnItems_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSpawnItems;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::NewProp_bSpawnItems_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::NewProp_bSpawnItems_SetBit(void* Obj)
	{
		((EndlessRunner_eventAddFloorTiles_Parms*)Obj)->bSpawnItems = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::NewProp_bSpawnItems = { "bSpawnItems", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(EndlessRunner_eventAddFloorTiles_Parms), &Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::NewProp_bSpawnItems_SetBit, METADATA_PARAMS(Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::NewProp_bSpawnItems_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::NewProp_bSpawnItems_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::NewProp_bSpawnItems,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AEndlessRunner, nullptr, "AddFloorTiles", nullptr, nullptr, sizeof(EndlessRunner_eventAddFloorTiles_Parms), Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AEndlessRunner_AddFloorTiles()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AEndlessRunner_AddFloorTiles_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AEndlessRunner_CreateIntialFloorTiles_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEndlessRunner_CreateIntialFloorTiles_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AEndlessRunner_CreateIntialFloorTiles_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AEndlessRunner, nullptr, "CreateIntialFloorTiles", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AEndlessRunner_CreateIntialFloorTiles_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_CreateIntialFloorTiles_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AEndlessRunner_CreateIntialFloorTiles()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AEndlessRunner_CreateIntialFloorTiles_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics
	{
		struct EndlessRunner_eventHitObstacle_Parms
		{
			ARunCharacter* RunCharacter;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RunCharacter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::NewProp_RunCharacter = { "RunCharacter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EndlessRunner_eventHitObstacle_Parms, RunCharacter), Z_Construct_UClass_ARunCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::NewProp_RunCharacter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AEndlessRunner, nullptr, "HitObstacle", nullptr, nullptr, sizeof(EndlessRunner_eventHitObstacle_Parms), Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AEndlessRunner_HitObstacle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AEndlessRunner_HitObstacle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics
	{
		struct EndlessRunner_eventOnTileExited_Parms
		{
			ARunCharacter* RunCharacter;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RunCharacter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::NewProp_RunCharacter = { "RunCharacter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(EndlessRunner_eventOnTileExited_Parms, RunCharacter), Z_Construct_UClass_ARunCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::NewProp_RunCharacter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AEndlessRunner, nullptr, "OnTileExited", nullptr, nullptr, sizeof(EndlessRunner_eventOnTileExited_Parms), Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AEndlessRunner_OnTileExited()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AEndlessRunner_OnTileExited_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AEndlessRunner_RestartLevel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEndlessRunner_RestartLevel_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AEndlessRunner_RestartLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AEndlessRunner, nullptr, "RestartLevel", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AEndlessRunner_RestartLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AEndlessRunner_RestartLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AEndlessRunner_RestartLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AEndlessRunner_RestartLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AEndlessRunner_NoRegister()
	{
		return AEndlessRunner::StaticClass();
	}
	struct Z_Construct_UClass_AEndlessRunner_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NextSpawnPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NextSpawnPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalCoins_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TotalCoins;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumInitialFloorTiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumInitialFloorTiles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloorTileClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_FloorTileClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEndlessRunner_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Gamprog2,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AEndlessRunner_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AEndlessRunner_AddCoin, "AddCoin" }, // 4218117956
		{ &Z_Construct_UFunction_AEndlessRunner_AddFloorTiles, "AddFloorTiles" }, // 3096500034
		{ &Z_Construct_UFunction_AEndlessRunner_CreateIntialFloorTiles, "CreateIntialFloorTiles" }, // 3262974275
		{ &Z_Construct_UFunction_AEndlessRunner_HitObstacle, "HitObstacle" }, // 857988478
		{ &Z_Construct_UFunction_AEndlessRunner_OnTileExited, "OnTileExited" }, // 143425153
		{ &Z_Construct_UFunction_AEndlessRunner_RestartLevel, "RestartLevel" }, // 335311592
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEndlessRunner_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "EndlessRunner.h" },
		{ "ModuleRelativePath", "EndlessRunner.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NextSpawnPoint_MetaData[] = {
		{ "Category", "Runtime" },
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NextSpawnPoint = { "NextSpawnPoint", nullptr, (EPropertyFlags)0x0010000000020801, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEndlessRunner, NextSpawnPoint), Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NextSpawnPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NextSpawnPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEndlessRunner_Statics::NewProp_TotalCoins_MetaData[] = {
		{ "Category", "EndlessRunner" },
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AEndlessRunner_Statics::NewProp_TotalCoins = { "TotalCoins", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEndlessRunner, TotalCoins), METADATA_PARAMS(Z_Construct_UClass_AEndlessRunner_Statics::NewProp_TotalCoins_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEndlessRunner_Statics::NewProp_TotalCoins_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NumInitialFloorTiles_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NumInitialFloorTiles = { "NumInitialFloorTiles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEndlessRunner, NumInitialFloorTiles), METADATA_PARAMS(Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NumInitialFloorTiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NumInitialFloorTiles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEndlessRunner_Statics::NewProp_FloorTileClass_MetaData[] = {
		{ "Category", "Config" },
		{ "ModuleRelativePath", "EndlessRunner.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AEndlessRunner_Statics::NewProp_FloorTileClass = { "FloorTileClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEndlessRunner, FloorTileClass), Z_Construct_UClass_AFloorTile_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AEndlessRunner_Statics::NewProp_FloorTileClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEndlessRunner_Statics::NewProp_FloorTileClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AEndlessRunner_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NextSpawnPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEndlessRunner_Statics::NewProp_TotalCoins,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEndlessRunner_Statics::NewProp_NumInitialFloorTiles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEndlessRunner_Statics::NewProp_FloorTileClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEndlessRunner_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEndlessRunner>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEndlessRunner_Statics::ClassParams = {
		&AEndlessRunner::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AEndlessRunner_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AEndlessRunner_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_AEndlessRunner_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AEndlessRunner_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEndlessRunner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEndlessRunner_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEndlessRunner, 2324946905);
	template<> GAMPROG2_API UClass* StaticClass<AEndlessRunner>()
	{
		return AEndlessRunner::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEndlessRunner(Z_Construct_UClass_AEndlessRunner, &AEndlessRunner::StaticClass, TEXT("/Script/Gamprog2"), TEXT("AEndlessRunner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEndlessRunner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
