// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2_Gamprog2GameMode_generated_h
#error "Gamprog2GameMode.generated.h already included, missing '#pragma once' in Gamprog2GameMode.h"
#endif
#define GAMPROG2_Gamprog2GameMode_generated_h

#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_SPARSE_DATA
#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_RPC_WRAPPERS
#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGamprog2GameMode(); \
	friend struct Z_Construct_UClass_AGamprog2GameMode_Statics; \
public: \
	DECLARE_CLASS(AGamprog2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), GAMPROG2_API) \
	DECLARE_SERIALIZER(AGamprog2GameMode)


#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAGamprog2GameMode(); \
	friend struct Z_Construct_UClass_AGamprog2GameMode_Statics; \
public: \
	DECLARE_CLASS(AGamprog2GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), GAMPROG2_API) \
	DECLARE_SERIALIZER(AGamprog2GameMode)


#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GAMPROG2_API AGamprog2GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGamprog2GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMPROG2_API, AGamprog2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGamprog2GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMPROG2_API AGamprog2GameMode(AGamprog2GameMode&&); \
	GAMPROG2_API AGamprog2GameMode(const AGamprog2GameMode&); \
public:


#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GAMPROG2_API AGamprog2GameMode(AGamprog2GameMode&&); \
	GAMPROG2_API AGamprog2GameMode(const AGamprog2GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GAMPROG2_API, AGamprog2GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGamprog2GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGamprog2GameMode)


#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_9_PROLOG
#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_RPC_WRAPPERS \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_INCLASS \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_INCLASS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_Gamprog2GameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2_API UClass* StaticClass<class AGamprog2GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamprog2_Source_Gamprog2_Gamprog2GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
