// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAMPROG2_RunCharacterController_generated_h
#error "RunCharacterController.generated.h already included, missing '#pragma once' in RunCharacterController.h"
#endif
#define GAMPROG2_RunCharacterController_generated_h

#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_SPARSE_DATA
#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_RPC_WRAPPERS
#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_INCLASS \
private: \
	static void StaticRegisterNativesARunCharacterController(); \
	friend struct Z_Construct_UClass_ARunCharacterController_Statics; \
public: \
	DECLARE_CLASS(ARunCharacterController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(ARunCharacterController)


#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARunCharacterController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARunCharacterController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public:


#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARunCharacterController(ARunCharacterController&&); \
	NO_API ARunCharacterController(const ARunCharacterController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARunCharacterController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARunCharacterController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARunCharacterController)


#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_PRIVATE_PROPERTY_OFFSET
#define Gamprog2_Source_Gamprog2_RunCharacterController_h_14_PROLOG
#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_RPC_WRAPPERS \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_INCLASS \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamprog2_Source_Gamprog2_RunCharacterController_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_INCLASS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_RunCharacterController_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2_API UClass* StaticClass<class ARunCharacterController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamprog2_Source_Gamprog2_RunCharacterController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
