// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ARunCharacter;
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef GAMPROG2_Obstacle_generated_h
#error "Obstacle.generated.h already included, missing '#pragma once' in Obstacle.h"
#endif
#define GAMPROG2_Obstacle_generated_h

#define Gamprog2_Source_Gamprog2_Obstacle_h_10_DELEGATE \
struct _Script_Gamprog2_eventHitObstacleSignature_Parms \
{ \
	ARunCharacter* RunCharacter; \
}; \
static inline void FHitObstacleSignature_DelegateWrapper(const FMulticastScriptDelegate& HitObstacleSignature, ARunCharacter* RunCharacter) \
{ \
	_Script_Gamprog2_eventHitObstacleSignature_Parms Parms; \
	Parms.RunCharacter=RunCharacter; \
	HitObstacleSignature.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Gamprog2_Source_Gamprog2_Obstacle_h_15_SPARSE_DATA
#define Gamprog2_Source_Gamprog2_Obstacle_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnObstacleHit);


#define Gamprog2_Source_Gamprog2_Obstacle_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnObstacleHit);


#define Gamprog2_Source_Gamprog2_Obstacle_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAObstacle(); \
	friend struct Z_Construct_UClass_AObstacle_Statics; \
public: \
	DECLARE_CLASS(AObstacle, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(AObstacle)


#define Gamprog2_Source_Gamprog2_Obstacle_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAObstacle(); \
	friend struct Z_Construct_UClass_AObstacle_Statics; \
public: \
	DECLARE_CLASS(AObstacle, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Gamprog2"), NO_API) \
	DECLARE_SERIALIZER(AObstacle)


#define Gamprog2_Source_Gamprog2_Obstacle_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AObstacle(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AObstacle) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacle); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacle(AObstacle&&); \
	NO_API AObstacle(const AObstacle&); \
public:


#define Gamprog2_Source_Gamprog2_Obstacle_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AObstacle(AObstacle&&); \
	NO_API AObstacle(const AObstacle&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AObstacle); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AObstacle); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AObstacle)


#define Gamprog2_Source_Gamprog2_Obstacle_h_15_PRIVATE_PROPERTY_OFFSET
#define Gamprog2_Source_Gamprog2_Obstacle_h_11_PROLOG
#define Gamprog2_Source_Gamprog2_Obstacle_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_RPC_WRAPPERS \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_INCLASS \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Gamprog2_Source_Gamprog2_Obstacle_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_PRIVATE_PROPERTY_OFFSET \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_SPARSE_DATA \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_INCLASS_NO_PURE_DECLS \
	Gamprog2_Source_Gamprog2_Obstacle_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAMPROG2_API UClass* StaticClass<class AObstacle>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Gamprog2_Source_Gamprog2_Obstacle_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
