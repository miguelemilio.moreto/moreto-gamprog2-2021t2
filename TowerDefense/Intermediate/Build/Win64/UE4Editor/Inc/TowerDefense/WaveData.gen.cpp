// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefense/WaveData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaveData() {}
// Cross Module References
	TOWERDEFENSE_API UScriptStruct* Z_Construct_UScriptStruct_FEnemySpawnConfig();
	UPackage* Z_Construct_UPackage__Script_TowerDefense();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	TOWERDEFENSE_API UClass* Z_Construct_UClass_AEnemyAi_NoRegister();
	TOWERDEFENSE_API UClass* Z_Construct_UClass_UWaveData_NoRegister();
	TOWERDEFENSE_API UClass* Z_Construct_UClass_UWaveData();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
// End Cross Module References
class UScriptStruct* FEnemySpawnConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TOWERDEFENSE_API uint32 Get_Z_Construct_UScriptStruct_FEnemySpawnConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEnemySpawnConfig, Z_Construct_UPackage__Script_TowerDefense(), TEXT("EnemySpawnConfig"), sizeof(FEnemySpawnConfig), Get_Z_Construct_UScriptStruct_FEnemySpawnConfig_Hash());
	}
	return Singleton;
}
template<> TOWERDEFENSE_API UScriptStruct* StaticStruct<FEnemySpawnConfig>()
{
	return FEnemySpawnConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEnemySpawnConfig(FEnemySpawnConfig::StaticStruct, TEXT("/Script/TowerDefense"), TEXT("EnemySpawnConfig"), false, nullptr, nullptr);
static struct FScriptStruct_TowerDefense_StaticRegisterNativesFEnemySpawnConfig
{
	FScriptStruct_TowerDefense_StaticRegisterNativesFEnemySpawnConfig()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("EnemySpawnConfig")),new UScriptStruct::TCppStructOps<FEnemySpawnConfig>);
	}
} ScriptStruct_TowerDefense_StaticRegisterNativesFEnemySpawnConfig;
	struct Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemySpawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EnemySpawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnemyCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EnemyCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Enemy_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_Enemy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEnemySpawnConfig>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemySpawn_MetaData[] = {
		{ "Category", "EnemySpawnConfig" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemySpawn = { "EnemySpawn", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemySpawnConfig, EnemySpawn), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemySpawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemySpawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemyCount_MetaData[] = {
		{ "Category", "EnemySpawnConfig" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemyCount = { "EnemyCount", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemySpawnConfig, EnemyCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemyCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemyCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_Enemy_MetaData[] = {
		{ "Category", "EnemySpawnConfig" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_Enemy = { "Enemy", nullptr, (EPropertyFlags)0x0014000000000015, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnemySpawnConfig, Enemy), Z_Construct_UClass_AEnemyAi_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_Enemy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_Enemy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemySpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_EnemyCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::NewProp_Enemy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefense,
		nullptr,
		&NewStructOps,
		"EnemySpawnConfig",
		sizeof(FEnemySpawnConfig),
		alignof(FEnemySpawnConfig),
		Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEnemySpawnConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEnemySpawnConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TowerDefense();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EnemySpawnConfig"), sizeof(FEnemySpawnConfig), Get_Z_Construct_UScriptStruct_FEnemySpawnConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEnemySpawnConfig_Hash() { return 3703773981U; }
	DEFINE_FUNCTION(UWaveData::execUnitCount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->UnitCount();
		P_NATIVE_END;
	}
	void UWaveData::StaticRegisterNativesUWaveData()
	{
		UClass* Class = UWaveData::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "UnitCount", &UWaveData::execUnitCount },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWaveData_UnitCount_Statics
	{
		struct WaveData_eventUnitCount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UWaveData_UnitCount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaveData_eventUnitCount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWaveData_UnitCount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWaveData_UnitCount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWaveData_UnitCount_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWaveData_UnitCount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWaveData, nullptr, "UnitCount", nullptr, nullptr, sizeof(WaveData_eventUnitCount_Parms), Z_Construct_UFunction_UWaveData_UnitCount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWaveData_UnitCount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWaveData_UnitCount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWaveData_UnitCount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWaveData_UnitCount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWaveData_UnitCount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UWaveData_NoRegister()
	{
		return UWaveData::StaticClass();
	}
	struct Z_Construct_UClass_UWaveData_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Enemies_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Enemies;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Enemies_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_waveTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_waveTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaveData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefense,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWaveData_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWaveData_UnitCount, "UnitCount" }, // 4188489385
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "WaveData.h" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::NewProp_Enemies_MetaData[] = {
		{ "Category", "WaveData" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_Enemies = { "Enemies", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveData, Enemies), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::NewProp_Enemies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::NewProp_Enemies_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_Enemies_Inner = { "Enemies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FEnemySpawnConfig, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaveData_Statics::NewProp_waveTime_MetaData[] = {
		{ "Category", "WaveData" },
		{ "ModuleRelativePath", "WaveData.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaveData_Statics::NewProp_waveTime = { "waveTime", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaveData, waveTime), METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::NewProp_waveTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::NewProp_waveTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaveData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_Enemies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_Enemies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaveData_Statics::NewProp_waveTime,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaveData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaveData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaveData_Statics::ClassParams = {
		&UWaveData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UWaveData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaveData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaveData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaveData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaveData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaveData, 2900927982);
	template<> TOWERDEFENSE_API UClass* StaticClass<UWaveData>()
	{
		return UWaveData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaveData(Z_Construct_UClass_UWaveData, &UWaveData::StaticClass, TEXT("/Script/TowerDefense"), TEXT("UWaveData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaveData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
