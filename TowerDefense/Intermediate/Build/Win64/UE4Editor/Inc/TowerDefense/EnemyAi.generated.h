// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSE_EnemyAi_generated_h
#error "EnemyAi.generated.h already included, missing '#pragma once' in EnemyAi.h"
#endif
#define TOWERDEFENSE_EnemyAi_generated_h

#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_SPARSE_DATA
#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAddPoints); \
	DECLARE_FUNCTION(execDeath);


#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAddPoints); \
	DECLARE_FUNCTION(execDeath);


#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyAi(); \
	friend struct Z_Construct_UClass_AEnemyAi_Statics; \
public: \
	DECLARE_CLASS(AEnemyAi, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefense"), NO_API) \
	DECLARE_SERIALIZER(AEnemyAi)


#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyAi(); \
	friend struct Z_Construct_UClass_AEnemyAi_Statics; \
public: \
	DECLARE_CLASS(AEnemyAi, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefense"), NO_API) \
	DECLARE_SERIALIZER(AEnemyAi)


#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyAi(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyAi) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyAi); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyAi); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyAi(AEnemyAi&&); \
	NO_API AEnemyAi(const AEnemyAi&); \
public:


#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyAi(AEnemyAi&&); \
	NO_API AEnemyAi(const AEnemyAi&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyAi); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyAi); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemyAi)


#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__unitHP() { return STRUCT_OFFSET(AEnemyAi, unitHP); } \
	FORCEINLINE static uint32 __PPO__unitSpeed() { return STRUCT_OFFSET(AEnemyAi, unitSpeed); } \
	FORCEINLINE static uint32 __PPO__isFlying() { return STRUCT_OFFSET(AEnemyAi, isFlying); } \
	FORCEINLINE static uint32 __PPO__isBoss() { return STRUCT_OFFSET(AEnemyAi, isBoss); } \
	FORCEINLINE static uint32 __PPO__CurrentWaypoint() { return STRUCT_OFFSET(AEnemyAi, CurrentWaypoint); }


#define TowerDefense_Source_TowerDefense_EnemyAi_h_9_PROLOG
#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_SPARSE_DATA \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_RPC_WRAPPERS \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_INCLASS \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefense_Source_TowerDefense_EnemyAi_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_PRIVATE_PROPERTY_OFFSET \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_SPARSE_DATA \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_INCLASS_NO_PURE_DECLS \
	TowerDefense_Source_TowerDefense_EnemyAi_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSE_API UClass* StaticClass<class AEnemyAi>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefense_Source_TowerDefense_EnemyAi_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
