// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSE_TowerDefenseGameModeBase_generated_h
#error "TowerDefenseGameModeBase.generated.h already included, missing '#pragma once' in TowerDefenseGameModeBase.h"
#endif
#define TOWERDEFENSE_TowerDefenseGameModeBase_generated_h

#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_SPARSE_DATA
#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_RPC_WRAPPERS
#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATowerDefenseGameModeBase(); \
	friend struct Z_Construct_UClass_ATowerDefenseGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefense"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenseGameModeBase)


#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATowerDefenseGameModeBase(); \
	friend struct Z_Construct_UClass_ATowerDefenseGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ATowerDefenseGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefense"), NO_API) \
	DECLARE_SERIALIZER(ATowerDefenseGameModeBase)


#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerDefenseGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenseGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenseGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenseGameModeBase(ATowerDefenseGameModeBase&&); \
	NO_API ATowerDefenseGameModeBase(const ATowerDefenseGameModeBase&); \
public:


#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATowerDefenseGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATowerDefenseGameModeBase(ATowerDefenseGameModeBase&&); \
	NO_API ATowerDefenseGameModeBase(const ATowerDefenseGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATowerDefenseGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATowerDefenseGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATowerDefenseGameModeBase)


#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_12_PROLOG
#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_SPARSE_DATA \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_RPC_WRAPPERS \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_INCLASS \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_SPARSE_DATA \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSE_API UClass* StaticClass<class ATowerDefenseGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefense_Source_TowerDefense_TowerDefenseGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
