// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSE_EnemyAiController_generated_h
#error "EnemyAiController.generated.h already included, missing '#pragma once' in EnemyAiController.h"
#endif
#define TOWERDEFENSE_EnemyAiController_generated_h

#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_SPARSE_DATA
#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_RPC_WRAPPERS
#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyAiController(); \
	friend struct Z_Construct_UClass_AEnemyAiController_Statics; \
public: \
	DECLARE_CLASS(AEnemyAiController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefense"), NO_API) \
	DECLARE_SERIALIZER(AEnemyAiController)


#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyAiController(); \
	friend struct Z_Construct_UClass_AEnemyAiController_Statics; \
public: \
	DECLARE_CLASS(AEnemyAiController, AAIController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TowerDefense"), NO_API) \
	DECLARE_SERIALIZER(AEnemyAiController)


#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyAiController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyAiController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyAiController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyAiController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyAiController(AEnemyAiController&&); \
	NO_API AEnemyAiController(const AEnemyAiController&); \
public:


#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyAiController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyAiController(AEnemyAiController&&); \
	NO_API AEnemyAiController(const AEnemyAiController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyAiController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyAiController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyAiController)


#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_PRIVATE_PROPERTY_OFFSET
#define TowerDefense_Source_TowerDefense_EnemyAiController_h_12_PROLOG
#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_PRIVATE_PROPERTY_OFFSET \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_SPARSE_DATA \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_RPC_WRAPPERS \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_INCLASS \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefense_Source_TowerDefense_EnemyAiController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_PRIVATE_PROPERTY_OFFSET \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_SPARSE_DATA \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_INCLASS_NO_PURE_DECLS \
	TowerDefense_Source_TowerDefense_EnemyAiController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSE_API UClass* StaticClass<class AEnemyAiController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefense_Source_TowerDefense_EnemyAiController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
