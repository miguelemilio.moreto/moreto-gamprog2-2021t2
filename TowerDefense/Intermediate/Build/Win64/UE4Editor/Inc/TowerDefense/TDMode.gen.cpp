// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefense/TDMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTDMode() {}
// Cross Module References
	TOWERDEFENSE_API UClass* Z_Construct_UClass_ATDMode_NoRegister();
	TOWERDEFENSE_API UClass* Z_Construct_UClass_ATDMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_TowerDefense();
// End Cross Module References
	DEFINE_FUNCTION(ATDMode::execCoreTakeDamage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CoreTakeDamage();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDMode::execEndLevel)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EndLevel();
		P_NATIVE_END;
	}
	void ATDMode::StaticRegisterNativesATDMode()
	{
		UClass* Class = ATDMode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CoreTakeDamage", &ATDMode::execCoreTakeDamage },
			{ "EndLevel", &ATDMode::execEndLevel },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATDMode_CoreTakeDamage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDMode_CoreTakeDamage_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDMode_CoreTakeDamage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDMode, nullptr, "CoreTakeDamage", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDMode_CoreTakeDamage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDMode_CoreTakeDamage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDMode_CoreTakeDamage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDMode_CoreTakeDamage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDMode_EndLevel_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDMode_EndLevel_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDMode_EndLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDMode, nullptr, "EndLevel", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDMode_EndLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDMode_EndLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDMode_EndLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDMode_EndLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATDMode_NoRegister()
	{
		return ATDMode::StaticClass();
	}
	struct Z_Construct_UClass_ATDMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Health_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Health;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Score_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Score;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATDMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefense,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATDMode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATDMode_CoreTakeDamage, "CoreTakeDamage" }, // 291807884
		{ &Z_Construct_UFunction_ATDMode_EndLevel, "EndLevel" }, // 3633371660
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDMode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "TDMode.h" },
		{ "ModuleRelativePath", "TDMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDMode_Statics::NewProp_Health_MetaData[] = {
		{ "Category", "TDMode" },
		{ "ModuleRelativePath", "TDMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ATDMode_Statics::NewProp_Health = { "Health", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDMode, Health), METADATA_PARAMS(Z_Construct_UClass_ATDMode_Statics::NewProp_Health_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDMode_Statics::NewProp_Health_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDMode_Statics::NewProp_Score_MetaData[] = {
		{ "Category", "TDMode" },
		{ "ModuleRelativePath", "TDMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ATDMode_Statics::NewProp_Score = { "Score", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDMode, Score), METADATA_PARAMS(Z_Construct_UClass_ATDMode_Statics::NewProp_Score_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDMode_Statics::NewProp_Score_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATDMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDMode_Statics::NewProp_Health,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDMode_Statics::NewProp_Score,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATDMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATDMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATDMode_Statics::ClassParams = {
		&ATDMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATDMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATDMode_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ATDMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATDMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATDMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATDMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATDMode, 1763240969);
	template<> TOWERDEFENSE_API UClass* StaticClass<ATDMode>()
	{
		return ATDMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATDMode(Z_Construct_UClass_ATDMode, &ATDMode::StaticClass, TEXT("/Script/TowerDefense"), TEXT("ATDMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATDMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
