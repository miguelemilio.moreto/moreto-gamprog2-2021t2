// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TowerDefense/EnemyAi.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnemyAi() {}
// Cross Module References
	TOWERDEFENSE_API UClass* Z_Construct_UClass_AEnemyAi_NoRegister();
	TOWERDEFENSE_API UClass* Z_Construct_UClass_AEnemyAi();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_TowerDefense();
// End Cross Module References
	DEFINE_FUNCTION(AEnemyAi::execAddPoints)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddPoints();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AEnemyAi::execDeath)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Death();
		P_NATIVE_END;
	}
	void AEnemyAi::StaticRegisterNativesAEnemyAi()
	{
		UClass* Class = AEnemyAi::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddPoints", &AEnemyAi::execAddPoints },
			{ "Death", &AEnemyAi::execDeath },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AEnemyAi_AddPoints_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEnemyAi_AddPoints_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "EnemyAi.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AEnemyAi_AddPoints_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AEnemyAi, nullptr, "AddPoints", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AEnemyAi_AddPoints_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AEnemyAi_AddPoints_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AEnemyAi_AddPoints()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AEnemyAi_AddPoints_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AEnemyAi_Death_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AEnemyAi_Death_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "EnemyAi.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AEnemyAi_Death_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AEnemyAi, nullptr, "Death", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AEnemyAi_Death_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AEnemyAi_Death_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AEnemyAi_Death()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AEnemyAi_Death_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AEnemyAi_NoRegister()
	{
		return AEnemyAi::StaticClass();
	}
	struct Z_Construct_UClass_AEnemyAi_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentWaypoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CurrentWaypoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isBoss_MetaData[];
#endif
		static void NewProp_isBoss_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isBoss;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_isFlying_MetaData[];
#endif
		static void NewProp_isFlying_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_isFlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_unitSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_unitSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_unitHP_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_unitHP;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AEnemyAi_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_TowerDefense,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AEnemyAi_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AEnemyAi_AddPoints, "AddPoints" }, // 1338814778
		{ &Z_Construct_UFunction_AEnemyAi_Death, "Death" }, // 3388081800
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEnemyAi_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "EnemyAi.h" },
		{ "ModuleRelativePath", "EnemyAi.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEnemyAi_Statics::NewProp_CurrentWaypoint_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "EnemyAi" },
		{ "ModuleRelativePath", "EnemyAi.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_AEnemyAi_Statics::NewProp_CurrentWaypoint = { "CurrentWaypoint", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEnemyAi, CurrentWaypoint), METADATA_PARAMS(Z_Construct_UClass_AEnemyAi_Statics::NewProp_CurrentWaypoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEnemyAi_Statics::NewProp_CurrentWaypoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEnemyAi_Statics::NewProp_isBoss_MetaData[] = {
		{ "Category", "Stats" },
		{ "ModuleRelativePath", "EnemyAi.h" },
	};
#endif
	void Z_Construct_UClass_AEnemyAi_Statics::NewProp_isBoss_SetBit(void* Obj)
	{
		((AEnemyAi*)Obj)->isBoss = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AEnemyAi_Statics::NewProp_isBoss = { "isBoss", nullptr, (EPropertyFlags)0x0020080000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AEnemyAi), &Z_Construct_UClass_AEnemyAi_Statics::NewProp_isBoss_SetBit, METADATA_PARAMS(Z_Construct_UClass_AEnemyAi_Statics::NewProp_isBoss_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEnemyAi_Statics::NewProp_isBoss_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEnemyAi_Statics::NewProp_isFlying_MetaData[] = {
		{ "Category", "Stats" },
		{ "ModuleRelativePath", "EnemyAi.h" },
	};
#endif
	void Z_Construct_UClass_AEnemyAi_Statics::NewProp_isFlying_SetBit(void* Obj)
	{
		((AEnemyAi*)Obj)->isFlying = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AEnemyAi_Statics::NewProp_isFlying = { "isFlying", nullptr, (EPropertyFlags)0x0020080000020005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AEnemyAi), &Z_Construct_UClass_AEnemyAi_Statics::NewProp_isFlying_SetBit, METADATA_PARAMS(Z_Construct_UClass_AEnemyAi_Statics::NewProp_isFlying_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEnemyAi_Statics::NewProp_isFlying_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitSpeed_MetaData[] = {
		{ "Category", "Stats" },
		{ "ModuleRelativePath", "EnemyAi.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitSpeed = { "unitSpeed", nullptr, (EPropertyFlags)0x0020080000020005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEnemyAi, unitSpeed), METADATA_PARAMS(Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitHP_MetaData[] = {
		{ "Category", "Stats" },
		{ "ModuleRelativePath", "EnemyAi.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitHP = { "unitHP", nullptr, (EPropertyFlags)0x0020080000020005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AEnemyAi, unitHP), METADATA_PARAMS(Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitHP_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitHP_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AEnemyAi_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEnemyAi_Statics::NewProp_CurrentWaypoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEnemyAi_Statics::NewProp_isBoss,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEnemyAi_Statics::NewProp_isFlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AEnemyAi_Statics::NewProp_unitHP,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AEnemyAi_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AEnemyAi>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AEnemyAi_Statics::ClassParams = {
		&AEnemyAi::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AEnemyAi_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AEnemyAi_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AEnemyAi_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AEnemyAi_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AEnemyAi()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AEnemyAi_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AEnemyAi, 2185696885);
	template<> TOWERDEFENSE_API UClass* StaticClass<AEnemyAi>()
	{
		return AEnemyAi::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AEnemyAi(Z_Construct_UClass_AEnemyAi, &AEnemyAi::StaticClass, TEXT("/Script/TowerDefense"), TEXT("AEnemyAi"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AEnemyAi);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
