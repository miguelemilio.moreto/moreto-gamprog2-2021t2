// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TOWERDEFENSE_WaveData_generated_h
#error "WaveData.generated.h already included, missing '#pragma once' in WaveData.h"
#endif
#define TOWERDEFENSE_WaveData_generated_h

#define TowerDefense_Source_TowerDefense_WaveData_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEnemySpawnConfig_Statics; \
	TOWERDEFENSE_API static class UScriptStruct* StaticStruct();


template<> TOWERDEFENSE_API UScriptStruct* StaticStruct<struct FEnemySpawnConfig>();

#define TowerDefense_Source_TowerDefense_WaveData_h_28_SPARSE_DATA
#define TowerDefense_Source_TowerDefense_WaveData_h_28_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUnitCount);


#define TowerDefense_Source_TowerDefense_WaveData_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUnitCount);


#define TowerDefense_Source_TowerDefense_WaveData_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWaveData(); \
	friend struct Z_Construct_UClass_UWaveData_Statics; \
public: \
	DECLARE_CLASS(UWaveData, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TowerDefense"), NO_API) \
	DECLARE_SERIALIZER(UWaveData)


#define TowerDefense_Source_TowerDefense_WaveData_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUWaveData(); \
	friend struct Z_Construct_UClass_UWaveData_Statics; \
public: \
	DECLARE_CLASS(UWaveData, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TowerDefense"), NO_API) \
	DECLARE_SERIALIZER(UWaveData)


#define TowerDefense_Source_TowerDefense_WaveData_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWaveData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWaveData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWaveData); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWaveData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWaveData(UWaveData&&); \
	NO_API UWaveData(const UWaveData&); \
public:


#define TowerDefense_Source_TowerDefense_WaveData_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWaveData(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWaveData(UWaveData&&); \
	NO_API UWaveData(const UWaveData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWaveData); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWaveData); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWaveData)


#define TowerDefense_Source_TowerDefense_WaveData_h_28_PRIVATE_PROPERTY_OFFSET
#define TowerDefense_Source_TowerDefense_WaveData_h_25_PROLOG
#define TowerDefense_Source_TowerDefense_WaveData_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefense_Source_TowerDefense_WaveData_h_28_PRIVATE_PROPERTY_OFFSET \
	TowerDefense_Source_TowerDefense_WaveData_h_28_SPARSE_DATA \
	TowerDefense_Source_TowerDefense_WaveData_h_28_RPC_WRAPPERS \
	TowerDefense_Source_TowerDefense_WaveData_h_28_INCLASS \
	TowerDefense_Source_TowerDefense_WaveData_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TowerDefense_Source_TowerDefense_WaveData_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TowerDefense_Source_TowerDefense_WaveData_h_28_PRIVATE_PROPERTY_OFFSET \
	TowerDefense_Source_TowerDefense_WaveData_h_28_SPARSE_DATA \
	TowerDefense_Source_TowerDefense_WaveData_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	TowerDefense_Source_TowerDefense_WaveData_h_28_INCLASS_NO_PURE_DECLS \
	TowerDefense_Source_TowerDefense_WaveData_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TOWERDEFENSE_API UClass* StaticClass<class UWaveData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TowerDefense_Source_TowerDefense_WaveData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
