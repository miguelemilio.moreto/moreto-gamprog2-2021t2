// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

USTRUCT(BlueprintType)
struct FEnemySpawnConfig
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<class AEnemyAi> Enemy;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 EnemyCount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 EnemySpawn;

};

UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY() 

protected:
	int32 SpawnNum = 0;

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float waveTime;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FEnemySpawnConfig> Enemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Points")
		int32 Killpoints;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Points")
		int32 Endpoints;

	UFUNCTION(BlueprintPure)
	int32 UnitCount();
	
};
