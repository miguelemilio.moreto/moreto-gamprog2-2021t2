// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemySpawner.generated.h"
#include "EnemyAi.h"

UCLASS()
class TOWERDEFENSE_API AEnemySpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawner();
	//array for waypoints
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	TArray<AActor*> Waypoints;

	UFUNCTION(BlueprintCallable)
	void spawnEnemies(TArray<TSubclassOf<class AEnemyAI>>);

	UFUNCTION()
	void NextEnemyTimer();

	UFUNCTION()
	void SpawnNextEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
