// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAi.h"
#include "TowerDefense.h"
#include "EnemyAiController.h"
#include <Runtime\Engine\Classes\Kismet\GameplayStatics.h>
#include "Waypoint.h"

// Sets default values
AEnemyAi::AEnemyAi()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyAi::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWaypoint::StaticClass(), Waypoints);
	MoveToWaypoints();
}

// Called every frame
void AEnemyAi::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemyAi::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
 
void AEnemyAi::MoveToWaypoints()
{
	AEnemyAiController* EnemyAiController = Cast<AEnemyAiController>(GetController());

	if (EnemyAiController)
	{
		if (CurrentWaypoint <= Waypoints.Num())
		{
			for (AActor* Waypoint : Waypoints)
			{
				AWaypoint* WaypointItr = Cast<AWaypoint>(Waypoint);

				if (WaypointItr)
				{
					if (WaypointItr->GetWaypointOrder() == CurrentWaypoint)
					{
						EnemyAiController->MoveToActor(WaypointItr, 5.f, false);
						CurrentWaypoint++;
						break;
					}
				}
			}
		}
	}
}

void AEnemyAi::Death()
{
}

void AEnemyAi::AddPoints()
{
}

