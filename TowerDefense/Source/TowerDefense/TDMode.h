// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDMode.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATDMode : public AGameModeBase
{
	GENERATED_BODY()

private:
	FTimerHandle TimerDelayHandle;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 waveNumber;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> SpawnPoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float timeInBetweenWaves;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Score = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Health = 0;

	UFUNCTION()
		void WaveStart();

	UFUNCTION()
		void EndWave();

	UFUNCTION()
	void EndLevel();

	UFUNCTION()
	void CoreTakeDamage();
	
};
