// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildableNode.h"

// Sets default values
ABuildableNode::ABuildableNode()
{

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(SceneComponent);


 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuildableNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABuildableNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

