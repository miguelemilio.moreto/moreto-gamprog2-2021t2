// Fill out your copyright notice in the Description page of Project Settings.


#include "TDCore.h"
#include "EnemyAi.h"

// Sets default values
ATDCore::ATDCore()
{

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(SceneComponent);


 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDCore::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDCore::OnObstacleHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	AEnemyAi* Enemy = Cast<AEnemyAi>(OtherActor);
	
	if (Enemy)
	{
		//broadcast
	}
}

